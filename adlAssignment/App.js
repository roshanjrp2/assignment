 import React, {useState, useEffect} from 'react';
 import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  SectionList,
  FlatList,
  TouchableOpacity,
} from 'react-native';

const DATA1 = [{key: 0,name:'Top Picks'},{key: 1,name:'Beverages'},{key: 2,name:'Coffees'},{key: 3,name:'Snacks'},{key: 4,name:'Appetizers'},{key: 5,name:'Desserts'},{key: 6,name:'Soup'}];

const DATA2 = [
  {
    key: 0,
    title: "Top Picks",
    data: [{name:"Pizza",price:200}, {name:"Burger",price:100}, {name:"French Fries",price:200}]
  },
  {
    key: 1,
    title: "Beverages",
    data: [{name:"Coffee",price:100}, {name:"Juice",price:150},{name:"Soft drinks",price:100}]
  },
  {
    key: 2,
    title: "Coffees",
    data: [{name:"Hot Coffee", price: 50},{name:"Ice Coffee", price: 100},{name:"Espresso", price: 200}]
  },
  {
    key: 3,
    title: "Snacks",
    data: [{ name:"Cheese Cake", price: 50 },{ name:"Ice Cream", price: 50 }]
  },
  {
    key: 4,
    title: "Appetizers",
    data: [{ name:"Potato Wedges", price: 100 },{ name:"Cheesy Garlic Bread", price: 100 }]
  },
  {
    key: 5,
    title: "Desserts",
    data: [{ name:"Ice Cream", price: 100 },{ name:"Chocolate Mouse", price: 100 }]
  },
  {
    key: 6,
    title: "Soup",
    data: [{ name:"Chicken Soup", price: 100 },{ name:"Pork soup", price: 100 },{ name:"Fish Soup", price: 100 }, { name:"Sweet Corn", price: 100 },{ name:"Sea food soup", price: 100 }]
  }
];

function App() {

  const [selectedIndex, setSelectedIndex] = useState(0);
  const [scrollkey, setScrollKey] = useState(0);
  const [clicked, setClicked] = useState(false);

  useEffect(() => {

  }, []);

  const onPress = (item,index) => {
    setClicked(true);
    setScrollKey(0);
    setSelectedIndex(index);
    scrollToSection(index);
  }

  const Item = ({ item }) => (
    <View style={styles.item}>
      <Text style={styles.title}>{item.name}</Text>
      <Text style={styles.text}>LKR {item.price}</Text>
    </View>
  );
  
  const scrollToSection = (index) => {
    sectionListRef.scrollToLocation({
      animated: true,
      sectionIndex: index,
      itemIndex: 0,
      viewPosition: 0
    });
  };
  
  const subCategoryRenderItem = ({ item,index }) => {
    return (
      <TouchableOpacity
        style={styles.roundedButton}
        onPress={() => onPress(item,index)}>
          <View
            style={(index == selectedIndex && clicked) || (index == scrollkey && !clicked) ? styles.topMenuRed : styles.topMenu}>
            <Text style={styles.textColorWhite}>{item.name}</Text>
          </View>
      </TouchableOpacity>
    );
  };

  const onCheckViewableItems = ({ viewableItems, changed }) => {
    setScrollKey(viewableItems[0] && viewableItems[0].section && viewableItems[0].section.key ? viewableItems[0].section.key : 0);
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.sectionMarginTop}>
        <FlatList
          data={DATA1}
          renderItem={subCategoryRenderItem}
          horizontal={true}
          ItemSeparatorComponent={() => <View style={{margin: 4}}/>}
          showsHorizontalScrollIndicator={false}
        />
      </View>
      <View style={styles.sectionMarginTop}>
        <SectionList
          sections={DATA2}
          keyExtractor={(item, index) => item + index}
          ref={ref => (sectionListRef = ref)}
          renderItem={({ item }) => <Item item ={item} />}
          renderSectionHeader={({ section }) => (
            <Text style={styles.header}>{ section.title }</Text>
          )}
          onViewableItemsChanged={onCheckViewableItems}
          viewabilityConfig={{
            itemVisiblePercentThreshold: 100
          }}
          onMomentumScrollEnd={(event) => { 
            setClicked(false);
          }}
        />
      </View>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    marginHorizontal: 16
  },
  item: {
    backgroundColor: "#cccccc",
    padding: 20,
    marginVertical: 8
  },
  header: {
    fontSize: 32,
    backgroundColor: "#fff"
  },
  title: {
    fontSize: 24
  },
  text: {
    fontSize: 18
  },
  topMenu: {
    padding: 16,
    backgroundColor: '#666666',
    borderRadius: 15
  },
  textColorWhite: {
    color: '#ffffff',
    fontSize: 18
  },
  topMenuRed: {
    padding: 16,
    backgroundColor: 'red',
    borderRadius: 15
  },
  roundedButton: {
    borderRadius: 15
  },
  sectionMarginTop: {
    marginTop: 10
  }
});

export default App;
